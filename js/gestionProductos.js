var productosObtenidos;

function getProducos(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function () {
          if(this.readyState == 4 && this.status === 200) {
            //  console.table(JSON.parse(request.responseText).value);

              productosObtenidos = request.responseText;
              procesarProductos();
          }
      };
  request.open("GET",url,true);
  request.send();
}

function procesarProductos(){
  var JSONproductos = JSON.parse(productosObtenidos);
  //alert(JSONproductos.value[0].ProductName);

  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONproductos.value.length; i++) {
    //console.log(JSONproductos.value[i].ProductName);
    var newFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONproductos.value[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONproductos.value[i].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONproductos.value[i].UnitsInStock;

    newFila.appendChild(columnaNombre);
    newFila.appendChild(columnaPrecio);
    newFila.appendChild(columnaStock);

    tbody.appendChild(newFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
