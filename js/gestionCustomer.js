var clientesObtenidos;

function getCustomer(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function () {
          if(this.readyState == 4 && this.status === 200) {
            //  console.table(JSON.parse(request.responseText).value);

            clientesObtenidos = request.responseText;
            procesarClientes();
          }
      };
  request.open("GET",url,true);
  request.send();
}

function procesarClientes(){
  console.log(clientesObtenidos);
  var JSONclientes = JSON.parse(clientesObtenidos);

  var divClientes = document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

 var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  for (var i = 0; i < JSONclientes.value.length; i++) {
    console.log(JSONclientes.value[i]);
    var newFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONclientes.value[i].ContactName;

    var columnaCity = document.createElement("td");
    columnaCity.innerText = JSONclientes.value[i].City;

    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    if(JSONclientes.value[i].Country!="UK"){
      imgBandera.src = rutaBandera + JSONclientes.value[i].Country + ".png";
    }else{
      imgBandera.src = rutaBandera + "United-Kingdom" + ".png";
    }
    imgBandera.classList.add("flag");

    columnaBandera.appendChild(imgBandera);


    newFila.appendChild(columnaNombre);
    newFila.appendChild(columnaCity);
    newFila.appendChild(columnaBandera);

    tbody.appendChild(newFila);
  }

  tabla.appendChild(tbody);
  divClientes.appendChild(tabla);

}
